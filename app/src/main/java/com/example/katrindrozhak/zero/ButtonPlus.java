package com.example.katrindrozhak.zero;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;

/**
 * Created by katrindrozhak on 08.11.16.
 */

public class ButtonPlus extends Button {

    private static final String TAG = "Button";

    public ButtonPlus(Context context) {
        super(context);
    }

    public ButtonPlus(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomButtonFont(context, attrs);
    }
    public ButtonPlus(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomButtonFont(context, attrs);
    }

    private void setCustomButtonFont(Context context, AttributeSet attrs) {
        TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.ButtonPlus);
        String customButtonFont = arr.getString(R.styleable.ButtonPlus_customButtonFont);
        setCustomButtonFont(context, customButtonFont);
        arr.recycle();
    }

    public boolean setCustomButtonFont(Context context, String asset){

        Typeface typeFace = null;

        try {
            typeFace = Typeface.createFromAsset(context.getAssets(), asset);
        } catch (Exception ex) {
            Log.e(TAG, "Could not get typeface: "+ex.getMessage());
            return false;
        }
        setTypeface(typeFace);
        return true;

    }

}
