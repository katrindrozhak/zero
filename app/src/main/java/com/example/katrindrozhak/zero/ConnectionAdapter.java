package com.example.katrindrozhak.zero;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by katrindrozhak on 11.11.16.
 */

public class ConnectionAdapter extends BaseAdapter {

    Context context;
    LayoutInflater layoutInflater;
    ArrayList<Connection> connections;

    public static class ViewHolder{
        TextView number;
        TextView title;
        CheckBox inversion;
        CheckBox prof;
        FloatingActionButton add;
    }

    public ConnectionAdapter(Context context, ArrayList<Connection> connections){
        this.context = context;
        this.connections = connections;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return connections.size();
    }

    @Override
    public Object getItem(int position) {
        return connections.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item, parent, false);
            holder = new ViewHolder();

            holder.number = (TextView)view.findViewById(R.id.tvNumber);
            holder.title = (TextView) view.findViewById(R.id.tvCreate);

            holder.inversion = (CheckBox)view.findViewById(R.id.check_box1);
            holder.prof = (CheckBox)view.findViewById(R.id.check_box2);

            holder.add = (FloatingActionButton)view.findViewById(R.id.btn_create);

            holder.add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(parent.getContext(), CreateActivity.class);
                    parent.getContext().startActivity(intent);
                }
            });

            view.setTag(holder);
        } else {
            holder = (ViewHolder)view.getTag();
        }

        Connection con = getConnection(position);

        //            ((TextView)view.findViewById(R.id.tvNumber)).setText(con.number);
        holder.number.setText(con.number);
        //            ((TextView)view.findViewById(R.id.tvCreate)).setText(con.name);
        holder.title.setText(con.name);


//        CheckBox checkInversion = (CheckBox)view.findViewById(R.id.check_box1);
//        checkInversion.setOnCheckedChangeListener(myCheckChangeListener);

        holder.inversion.setOnCheckedChangeListener(myCheckChangeListener);
        holder.inversion.setTag(position);
//        checkInversion.setTag(position);
        holder.inversion.setChecked(con.inversionCheck);
//        checkInversion.setChecked(con.inversionCheck);

//        CheckBox checkProf = (CheckBox)view.findViewById(R.id.check_box2);
//        checkProf.setOnCheckedChangeListener(secondCheckChangeListener);
        holder.prof.setOnCheckedChangeListener(secondCheckChangeListener);
        holder.prof.setTag(position);
//        checkProf.setTag(position);
        holder.prof.setChecked(con.profCheck);
//        checkProf.setChecked(con.profCheck);
        return view;
    }

    private Connection getConnection(int position) {
        return ((Connection) getItem(position));
    }

    CompoundButton.OnCheckedChangeListener myCheckChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            getConnection((Integer) buttonView.getTag()).inversionCheck = isChecked;
        }
    };

    CompoundButton.OnCheckedChangeListener secondCheckChangeListener =  new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            getConnection((Integer) buttonView.getTag()).profCheck = isChecked;
        }
    };
}
