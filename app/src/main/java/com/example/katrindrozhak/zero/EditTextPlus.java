package com.example.katrindrozhak.zero;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

/**
 * Created by katrindrozhak on 09.11.16.
 */

public class EditTextPlus extends EditText {

    private static final String TAG = "Edit Text";

    public EditTextPlus(Context context) {
        super(context);
    }

    public EditTextPlus(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomEditFont(context, attrs);
    }

    public EditTextPlus(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomEditFont(context, attrs);
    }

    private void setCustomEditFont(Context context, AttributeSet attrs) {
        TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.EditTextPlus);
        String customEditTextFont = arr.getString(R.styleable.EditTextPlus_customEditFont);
        setCustomEditFont(context, customEditTextFont);
        arr.recycle();
    }

    public boolean setCustomEditFont(Context context, String asset) {
        Typeface tf = null;
        try {
            tf = Typeface.createFromAsset(context.getAssets(), asset);
        } catch (Exception ex) {
            Log.e(TAG, "Could not get typeface: "+ex.getMessage());
            return false;
        }
        setTypeface(tf);
        return true;
    }

}
