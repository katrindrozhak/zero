package com.example.katrindrozhak.zero;

import android.support.design.widget.FloatingActionButton;
import android.widget.Button;

/**
 * Created by katrindrozhak on 11.11.16.
 */

public class Connection {

    String number;
    String name;
    boolean inversionCheck;
    boolean profCheck;
    FloatingActionButton createNew;

    public Connection(String number,String name, boolean inversionCheck, boolean profCheck, FloatingActionButton createNew) {
        this.number = number;
        this.name = name;
        this.inversionCheck = inversionCheck;
        this.profCheck = profCheck;
        this.createNew = createNew;

    }
}
