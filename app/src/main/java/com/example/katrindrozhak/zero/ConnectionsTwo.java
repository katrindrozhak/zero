package com.example.katrindrozhak.zero;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class ConnectionsTwo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connections_two);

        ImageButton buttonList = (ImageButton) findViewById(R.id.btn_list);
        buttonList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConnectionsTwo.this, ConnectionThree.class);
                startActivity(intent);
            }
        });


        FloatingActionButton fabFive = (FloatingActionButton) findViewById(R.id.fab5);
        fabFive.setBackgroundResource(R.drawable.fab);
        fabFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                Intent intent = new Intent(ConnectionsTwo.this, CreateActivity.class);
                startActivity(intent);
            }
        });
    }

    public void onShowSettings(View view) {
        Toast toast = Toast.makeText(this, "Show settings", Toast.LENGTH_SHORT);
        toast.show();
    }
}
