package com.example.katrindrozhak.zero;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;


/**
 * Created by katrindrozhak on 03.11.16.
 */

public class ZeroActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.zero_activity);
    }

    public void onSignUp(View view) {
        Intent signUpIntent = new Intent(ZeroActivity.this, SignUp.class);
        startActivity(signUpIntent);
    }

    public void onLogIn(View view) {

        Intent intent = new Intent(this, ConnectionsTwo.class);
        startActivity(intent);
    }
}
