package com.example.katrindrozhak.zero;

import android.app.FragmentManager;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Toast;

public class ConnectionActivity extends AppCompatActivity {

    private TextViewPlus textViewPlus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection);

        textViewPlus = (TextViewPlus) findViewById(R.id.profound);
        Spannable span = new SpannableString(getResources().getString(R.string.btn_profound_encryption_off));
        span.setSpan(new ForegroundColorSpan(Color.WHITE), 0, 23, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textViewPlus.setText(span);
    }

    public void onSave(View view) {

        Toast toast = Toast.makeText(this, "Saved", Toast.LENGTH_SHORT);
        toast.show();

    }

    public void onUpdate(View view) {

        FragmentManager fragmentManager = getFragmentManager();
        DialogUpdateFragment dialogUpdateFragment = new DialogUpdateFragment();
        dialogUpdateFragment.show(fragmentManager, "dialog");
    }

    public void onCloseConnection(View view) {

        FragmentManager fragmentManager = getFragmentManager();
        ExitConnectionFragment exitConnectionFragment = new ExitConnectionFragment();
        exitConnectionFragment.show(fragmentManager, "dialog");

    }

    public void onDeleteButton(View view) {
        Toast toast = Toast.makeText(this, "Deleted", Toast.LENGTH_SHORT);
        toast.show();
    }
}
