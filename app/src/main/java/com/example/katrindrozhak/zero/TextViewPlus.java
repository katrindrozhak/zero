package com.example.katrindrozhak.zero;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

/**
 * Created by katrindrozhak on 08.11.16.
 */

public class TextViewPlus extends TextView {

    private static final String TAG = "Text View";

    public TextViewPlus(Context context) {
        super(context);
    }

    public TextViewPlus(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public TextViewPlus(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context context, AttributeSet attrs) {
        TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.TextViewPlus);
        String customFont = arr.getString(R.styleable.TextViewPlus_customFont);
        setCustomFont(context,  customFont);
        arr.recycle();

    }

    public boolean setCustomFont(Context context, String asset) {
        Typeface tf = null;

        try {
            tf = Typeface.createFromAsset(context.getAssets(), asset);
        } catch (Exception ex) {
            Log.e(TAG, "Could not get typeface: "+ex.getMessage());
            return false;
        }
        setTypeface(tf);
        return true;
    }
}
