package com.example.katrindrozhak.zero;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ConnectionThree extends AppCompatActivity {

    ArrayList<Connection> listConnections = new ArrayList<>();
    ConnectionAdapter connectionAdapter;
    FloatingActionButton button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_three);

        fillData();
        connectionAdapter = new ConnectionAdapter(this, listConnections);

        ListView lvMain = (ListView) findViewById(R.id.listView);
        lvMain.setAdapter(connectionAdapter);
    }

    private void fillData() {
        for (int i = 1; i <= 10; i++) {
            listConnections.add(new Connection(" " + i, " + ", false, false, null));
        }
    }

    public void onSettingsConnections(View view) {
        Toast toast = Toast.makeText(this, "Show settings", Toast.LENGTH_SHORT);
        toast.show();
    }

    public void onBackConnections(View view) {
        onBackPressed();
    }
}
